if (customwidgets == null || customwidgets == "undefined") {
    var customwidgets = {};
}
customwidgets.ProgressBar = {};
customwidgets.ProgressBar.currobj = null;
customwidgets.ProgressBar.init = function(pobject) {
	customwidgets.ProgressBar.currobj = pobject;
	var currobjary = designer.selectedobjs;
	var currobjspntr = new Array();
	for (var i = 0; i < currobjary.length; i++) {
		currobjspntr[i] = utils.getDataPointer(currobjary[i]);
	}
	//Apz312 changes start
	$("#mainPropCollapse").click(function(){
		$("#mainPropAccordian").toggle();
	});
	$("#addcollapse").click(function(){
		$("#addaccordian").toggle();
	});

	$("#eventscollapse").click(function(){
		$("#eventsaccordian").toggle();
	});
	//Apz312 changes end
	$("#title").blur(function() {
		ProgressBar_title();
	});
	$("#controlid").blur(function() {
		for (var i = 0; i < currobjary.length; i++) {
		$("#id").val(currobjary[i].id);
		var ldatapointer = currobjspntr[i];
		widgetutils.setProperty("controlid", ldatapointer);
}
});
	$("#options").change(function() {
		var lval = $("#options").prop("checked") ? "Y" : "N";
		$("#options")[0].value = lval;
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("options", ldatapointer);
				try {
					widgetutils.setWidgetVisibility(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		widgetutils.setWidgetVisibility();
		}
	});
	
	$("#headeralignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("headeralignment", ldatapointer);
				try {
					ProgressBar_headerAlignment(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		ProgressBar_headerAlignment();
		}
	});
	$("#columnalignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("columnalignment", ldatapointer);
			}
		}
	});
	$("#labelwidth").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("labelwidth", ldatapointer);
				try {
					ProgressBar_labelwidth(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		ProgressBar_labelwidth(ldatapointer);
		}
	});
	$("#labelalignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("labelalignment", ldatapointer);
				try {
					ProgressBar_labelalign(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		ProgressBar_labelalign(ldatapointer);
		}
	});
	$("#defaultvalue").blur(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("defaultvalue", ldatapointer);
				try {
					ProgressBar_defaultvalue();
				} catch (e) {}
			}
		}
		else{
		ProgressBar_defaultvalue();
		}
	});
	$("#cssclasses").blur(function () {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("cssclasses", ldatapointer);
			}
		}
	});
	$("#selectall").click(function() {
		widgetutils.setMultipleCheckboxes(this, "selectevent");
	});
	$("#variations").change(function() {
		ProgressBar_variations();
	});
	//Apz312 changes start
	//Apz312 changes end
}

customwidgets.ProgressBar.getTemplate = function(pobject, pparentobject,receviedForChild,uiParentObj) {
	var widgethtml = "";
    var parentContainer = utils.getContainer(pparentobject).getAttribute("widgettype");
    var parentContId = utils.getContainer(pparentobject).id;
    if(parentContainer == "TABLE") {
        var header = $("#"+parentContId).find("#"+parentContId+"_header");
		var newHeader = "<th onclick='widgetutils.selectTableElement(this, event);'>Progress Bar</th>";
		var parentContainer = utils.getContainer(pparentobject);
		if(receviedForChild) {
			var $uiParentObj = $(uiParentObj);
			var parentIndex = $uiParentObj.parent().children().index(uiParentObj);
			var uiParentHeader = header.children()[parentIndex];
			$(uiParentHeader).before(newHeader);
		} else{
			header.append(newHeader);
		}
		widgethtml = "<td id ='appzillonid' class='apzlocontainer'>";
		var elmContent = "<div class='progress elmWidget'><div class='determinate' style='width: 30%;'></div></div>";
		widgethtml = widgethtml + elmContent;
		widgethtml = widgethtml + "</td>";
    } else if(parentContainer == "LIST" || parentContainer == "NAVBAR") {
        widgethtml = '<div class="eobox" id ="appzillonid"  widgetcategory="elements" widgetid="ProgressBar" tabindex="0" ><div class="progress elmWidget"><div class="determinate" style="width: 30%;"></div></div></div>';
    } else {
        if(pparentobject.hasAttribute("role")) {
            var receivedTemplate = '<div class="progress elmWidget"><div class="determinate" style="width: 30%;"></div></div>';
			widgethtml = '<li id ="appzillonid" class="apzlocontainer elpad">';
			widgethtml = widgethtml + receivedTemplate;
			widgethtml = widgethtml + '</li>';
        } else {
            widgethtml = '<ul id ="appzillonid" class="apzlocontainer srow eobox wrapped"><li id="label" class="w40">Progress Bar</li><li id="content" class="w60 ebox"><div class="progress elmWidget"><div class="determinate" style="width: 30%;"></div></div></li></ul>';
        }
    }
	return widgethtml;
}

function getHTML(pproperties) {
    var lcontent = JSON.parse(pproperties), cssCls = "", shownoneCls = "", toolTip = "", tooltipCls = "";
    var defVal = (lcontent.defaultvalue)*100;
    if (lcontent.tooltip) {
        toolTip = " original-title="+lcontent.tooltip+"";
        tooltipCls = " tooltipcls";
    }
    if (lcontent.options == "N") {
        shownoneCls = " shownone";
     }
     if (lcontent.cssclasses != undefined) {
         cssCls = " " + lcontent.cssclasses;
     }
    var html = '<div class="progress '+shownoneCls+cssCls+tooltipCls+'" '+toolTip+' aria-labelledby=""><div class="'+lcontent.variations.toLowerCase()+'" style="width:'+defVal+'%"></div></div>';
    return html;
}

customwidgets.ProgressBar.updateDefaultValues = function(pobject, pparentobject) {
     document.getElementById("title").value ="ProgressBar";
     document.getElementById("defaultvalue").value ="0.30";
}

customwidgets.ProgressBar.getObject = function(pobject, pparentobject, pproperties) {
	widgethtml = "<div class='control-group' id ='appzillonid'  widgetcategory='elements' widgetid='badge' tabindex='0' ><ul class='col-ul'><li id='width1' style='width:20px' class='v-middle'><i id='icon1' class='m-icon-swapright'></i></li></ul></div>";
	return $(widgethtml)[0];
}

customwidgets.ProgressBar.populateProperties = function(pproperties) {

	widgetutils.getProperty("id", pproperties);
	widgetutils.getProperty("pid", pproperties);
	widgetutils.getProperty("title", pproperties);
	widgetutils.getProperty("controlid", pproperties);
	ProgressBar_displayElementsProperties(pproperties);
	widgetutils.getProperty("tooltip", pproperties);
	widgetutils.getProperty("headeralignment", pproperties);
	widgetutils.getProperty("columnalignment", pproperties);
	widgetutils.getProperty("options", pproperties);
	widgetutils.getProperty("labelwidth", pproperties);
	widgetutils.getProperty("tooltip", pproperties);
	widgetutils.getProperty("labelalignment", pproperties);
	widgetutils.getProperty("variations", pproperties);
	widgetutils.getProperty("hint", pproperties);
	widgetutils.getProperty("cssclasses", pproperties);
	widgetutils.getProperty("defaultvalue", pproperties);
	widgetutils.getEventElements("eventstable", pproperties);

}

customwidgets.ProgressBar.saveProperties = function(pproperties) {
	
	widgetutils.setProperty("id", pproperties);
	widgetutils.setProperty("pid", pproperties);
	widgetutils.setProperty("title", pproperties);
	widgetutils.setProperty("controlid", pproperties);
	widgetutils.setProperty("headeralignment", pproperties);
	widgetutils.setProperty("columnalignment", pproperties);
	widgetutils.setProperty("options", pproperties);
	widgetutils.setProperty("labelwidth", pproperties);
	widgetutils.setProperty("tooltip", pproperties);
	widgetutils.setProperty("labelalignment", pproperties);
	widgetutils.setProperty("variations", pproperties);
	widgetutils.setProperty("hint", pproperties);
	widgetutils.setProperty("cssclasses", pproperties);
	widgetutils.setProperty("defaultvalue", pproperties);
	widgetutils.setEvent("eventstable", pproperties);

}

function ProgressBar_displayElementsProperties(pproperties) {
    var id = document.getElementById("id").value;
	var parentContainer = utils.getContainer($("#"+id)[0]).getAttribute("widgettype");
	if (parentContainer == "TABLE") {
		$(".forTable").removeClass("shownone");
	}
	if (parentContainer == "NAVBAR") {
		$(".forNavbar").removeClass("shownone");
	}
	if (parentContainer == "FORM") {
		$(".forForm").removeClass("shownone");
	}
	//// fix 1648
	if (parentContainer == "LIST") {
		$(".forList").removeClass("shownone");
	}
	if(pproperties.draggable == "Y"){
		$(".draggableid").removeClass("shownone");
	} else {
		$(".draggableid").addClass("shownone");
	}
	if(pproperties) {
		var interfacename = pproperties.interfacename;
		var datamodeltyp = pproperties.datamodeltype;
		var nodename = pproperties.nodename;
		var elementname = pproperties.elementname;
	} else {
		var interfacename = document.getElementById("interfacename").value;
		var datamodeltyp = document.getElementById("datamodeltype").value;
		var nodename = document.getElementById("nodename").value;
		var elementname = document.getElementById("elementname").value;
	}
	if(utils.isDmlObj(interfacename,datamodeltyp, nodename, elementname)) {
		$(".handleso").addClass("disabled");
	} else {
		$(".handleso").removeClass("disabled");
	}
}

function ProgressBar_title(pobject) {
	if(pobject != undefined) {
        var res = pobject.id;
        var se = pobject.title ? pobject.title : "";
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("title").value;
    }
    se = widgetutils.getLITValue(se);
    var parentContainer = utils.getContainer($("#"+res)[0]);
    var parentContainerType = parentContainer.getAttribute("widgettype");
    if(parentContainerType != "TABLE") {
        var parentId = $("#"+res)[0].getAttribute("parentid");
        var parentobject = $("#" + parentId);
        if(parentobject[0].hasAttribute("role")) {
        } else
            $("#" + res).find("#label").html(se);
    } else {
        if(utils.isNull(se))
        	$("#" + res+"_heading").html("&nbsp;");
        else
        	$("#" + res+"_heading").html(se);
    }
}

function ProgressBar_headerAlignment(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.headeralignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("headeralignment").value;
	}
	var index = $("#" + res).parent("tr").children().index($("#"+res));
	var temp = $("#"+($("#"+res).closest("tbody").attr("parentid")+"_header")).children()[index];
	if(se == "LEFT")
		$(temp).removeClass('tright tcenter').addClass('tleft');
	else if(se == "RIGHT")
		$(temp).removeClass('tleft tcenter').addClass('tright');
	else 
		$(temp).removeClass('tright tleft').addClass('tcenter');
}

function ProgressBar_labelwidth (pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelwidth;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelwidth").value;
	}
	if(!utils.isNull(se) && $.isNumeric(se)){
		var ae = 100 - se;
	}else{
		var ae = 100;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
			var tem = $("#" + res).find("#content");
			if(!utils.isNull(se)) {
				$(temp).removeClass(classarray.join(' ')).addClass('w' + se);
				$(tem).removeClass(classarray.join(' ')).addClass('w' + ae);
			} else {
				$(temp).removeClass(classarray.join(' ')).addClass('w40');
				$(tem).removeClass(classarray.join(' ')).addClass('w60');
			}
		}
	}	
}

function ProgressBar_labelalign(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelalignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelalignment").value;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
        	if(se == "LEFT")
        		$(temp).removeClass('tright tcenter').addClass('tleft');
        	else if(se == "RIGHT")
        		$(temp).removeClass('tleft tcenter').addClass('tright');
        	else if(se == "CENTER")
        		$(temp).removeClass('tright tleft').addClass('tcenter');
        	else
        		$(temp).removeClass('tright tcenter tleft');
		}
	}
}

function ProgressBar_defaultvalue(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.defaultvalue;
		var varOpt = pobject.variations;
	} else {
		var se = document.getElementById("defaultvalue").value;
		var res = document.getElementById("id").value;
		var varOpt = document.getElementById("variations").value;
	}
	var parContnr = utils.getContainer($("#"+res)[0]);
	var parContnrType = parContnr.getAttribute("widgettype");
	var temp = $("#" + res).find(".determinate");
	if (temp.length==0){
		temp = $("#" + res).find(".indeterminate");
	}
	if(varOpt == "DETERMINATE") {
		if(!isNaN(se)){
			se = se*100;
			$(temp[0]).css('width',se+"%");
	    }
	}
}
function ProgressBar_variations(pobject) {
	if(pobject != undefined) {
		var pid = pobject.id;
		var varOpt = pobject.variations;
	} else {
		var pid = document.getElementById("id").value;
		var varOpt = document.getElementById("variations").value;
	}
	var parContnr = utils.getContainer($("#"+pid)[0]);
	var parContnrType = parContnr.getAttribute("widgettype");
	if(parContnrType == "FORM" || parContnrType == "TABLE") {
		var temp = $("#" + pid).find(".elmWidget");
	} else {
		var temp = $("#"+pid);
	}
	if(varOpt == "DETERMINATE") {
       $(temp[0]).find('.indeterminate').switchClass('indeterminate','determinate');
	} else {
       $(temp[0]).find('.determinate').switchClass('determinate','indeterminate');
	}
	ProgressBar_defaultvalue(pobject);
}

customwidgets.ProgressBar.applyProperties = function(pobject) {
	var parentContainer = utils.getContainer($("#"+pobject.id)[0]).getAttribute("widgettype");
	ProgressBar_title(pobject);
	widgetutils.setWidgetVisibility(pobject);
	ProgressBar_variations(pobject);
	if(parentContainer == "TABLE") {
		ProgressBar_headerAlignment(pobject);
	}
	if(parentContainer == "FORM") {
		ProgressBar_labelwidth(pobject);
		ProgressBar_labelalign(pobject);
	}
}
