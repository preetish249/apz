/************************************************************************
 Below are the widget related objects initializations needs to be provided at the time of loding the js.
 ************************************************************************/
if (customwidgets == null || customwidgets == "undefined") {
 var customwidgets = {};
}
customwidgets.SortCode = {};
customwidgets.SortCode.currobj = null;

/************************************************************************
 Below function is called from layout.loadPropPanel() to initialize the widget related functions.
 Functions initializations are done here.
 Functions here are called on certain events like onblur or onchange of the widget properties.
 Consider the widget have a property as Title with id as title
 Then the initialization looks like this :
 $("#title").blur(function() {
 changeTitle();
 });
 Provided the function changeTitle() in the js which changes the title of the widget
 ************************************************************************/
customwidgets.SortCode.init = function(pobject) {
	customwidgets.SortCode.currobj = pobject;
	var currobjary = designer.selectedobjs;
	var currobjspntr = new Array();
	for (var i = 0; i < currobjary.length; i++) {
		currobjspntr[i] = utils.getDataPointer(currobjary[i]);
	}
	//Apz312 changes start
	$("#mainPropCollapse").click(function(){
		$("#mainPropAccordian").toggle();
	});
	$("#additionalpropcollapse").click(function(){
		$("#additionalprop").toggle();
	});
	$("#eventscollapse").click(function(){
		$("#eventsaccordian").toggle();
	});
	$("#title").blur(function() {
		SortCode_title();
	});
	$("#width").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("width", ldatapointer);
				try {
					SortCode_colwidth(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_colwidth();
		}
	});
	$("#customwidth").blur(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("customwidth", ldatapointer);
				try {
					SortCode_customwidth(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_customwidth();
		}
	});
	$("#customwidthtype").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("customwidthtype", ldatapointer);
				try {
					SortCode_customwidth(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_customwidth();
		}
	});
	$("#labelwidth").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("labelwidth", ldatapointer);
				try {
					SortCode_labelwidth(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_labelwidth();
		}
	});
	$("#cssclasses").blur(function () {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("cssclasses", ldatapointer);
			}
		}
	});
	$("#contentalignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("contentalignment", ldatapointer);
				try {
					SortCode_conallign(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_conallign();
		}
	});
	$("#labelalignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("labelalignment", ldatapointer);
				try {
					SortCode_labelallign(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_labelallign();
		}
	});
	$("#state").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("state", ldatapointer);
				try {
					SortCode_state(ldatapointer);
					
				} catch (e) {}
			}
		}
		else{
		SortCode_state();
		}
	});
	$("#defaultvalue").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("defaultvalue", ldatapointer);
				try {
					SortCode_defaultvalue(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		SortCode_defaultvalue();
		}
	});
	$("#options").change(function() {
		var lval = $("#options").prop("checked") ? "Y" : "N";
		$("#options")[0].value = lval;
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("options", ldatapointer);
				try {
					widgetutils.setWidgetVisibility(ldatapointer);
				} catch (e) {}
			}
		}
		else{
		widgetutils.setWidgetVisibility();
		}
	});

     $("#headeralignment").change(function() {
        var lval = $("#headeralignment").prop("checked") ? "Y" : "N";
        $("#headeralignment")[0].value = lval;
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("headeralignment", ldatapointer);
                try {
                    SortCode_headerAlign(ldatapointer);
                } catch (e) {}
            }
        } else {
            SortCode_headerAlign();
        }
    });
	//Apz312 changes end
	$("#selectall").click(function() {
		widgetutils.setMultipleCheckboxes(this, "selectevent");
	});
	$("#placeholder").blur(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("placeholder", ldatapointer);
				try {
					SortCode_placeholder(ldatapointer);
				} catch (e) {}
			}
		} else{
		    SortCode_placeholder();
		}
	});
};

/************************************************************************
 Below function is called from layout.addWidget() to get the widget template provided by the developer.
 Provided widgethtml content will be displayed inside the Designer screen.
 Keep the provided outer div (with id appzillonid) as it contains widget related initializations, and keep your content inside that div.
 ************************************************************************/
customwidgets.SortCode.getTemplate = function(pobject, pparentobject,receviedForChild,uiParentObj) {
    var widgethtml = "";
    var parentContainer = utils.getContainer(pparentobject).getAttribute("widgettype");
    var parentContId = utils.getContainer(pparentobject).id;
    if(parentContainer == "TABLE") {
        var header = $("#"+parentContId).find("#"+parentContId+"_header");
		var newHeader = "<th onclick='widgetutils.selectTableElement(this, event);'>Sort Code</th>";
		var parentContainer = utils.getContainer(pparentobject);
		if(receviedForChild) {
			var $uiParentObj = $(uiParentObj);
			var parentIndex = $uiParentObj.parent().children().index(uiParentObj);
			var uiParentHeader = header.children()[parentIndex];
			$(uiParentHeader).before(newHeader);
		} else{
			header.append(newHeader);
		}
		widgethtml = "<td id ='appzillonid' class='apzlocontainer'>";
		var elmContent = '<div class="eboxh"><ul class="hrow sortcode"><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li></ul></div>';
		widgethtml = widgethtml + elmContent;
		widgethtml = widgethtml + "</td>";
    } else if(parentContainer == "LIST" || parentContainer == "NAVBAR") {
        widgethtml = '<div class="eobox" id ="appzillonid"  widgetcategory="elements" widgetid="SortCode" tabindex="0"><ul class="hrow sortcode"><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li></ul></div>';
    } else {
        if(pparentobject.hasAttribute("role")) {
            var receivedTemplate = '<ul class="hrow sortcode"><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li></ul>';
			widgethtml = '<li id ="appzillonid" class="apzlocontainer elpad">';
			widgethtml = widgethtml + receivedTemplate;
			widgethtml = widgethtml + '</li>';
        } else {
            widgethtml = '<ul id ="appzillonid" class="apzlocontainer srow eobox wrapped"><li id="label" class="w40">Sort Code</li><li id="content" class="w60 ebox"><ul class="hrow sortcode"><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li><li style="width:10px" class="tcenter"> - </li><li style="width:4em"><input type="text" id="sortcodeinput" class="w100  apz-element" placeholder="Value3"></li></ul></li></ul>';
        }
    }
	return widgethtml;
};

customwidgets.SortCode.updateDefaultValues = function(pobject, pparentobject) {
	document.getElementById("title").value ="Sort Code";
}
/************************************************************************
 Below function is called from layout.populateProperties() to populate the widget properties provided by the developer
 layout.populateProperties() will be called when the widget has been selected in the designer.
 Thats when the widget properties page will be populated.
	
 Send the widgets property id to the widgetutils.getPropety() function with 'pproperties' object
 So that the value stored in 'pproperties' object will be assigned to the element in the screen with the id which has been passed.
 Example: In below function id and pid are passed to the widgetutils.getProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.SortCode.populateProperties = function(pproperties) {
	widgetutils.getProperty("id", pproperties);
	widgetutils.getProperty("pid", pproperties);
	widgetutils.getProperty("title", pproperties);
	SortCode_displayElementsProperties(pproperties);
	widgetutils.getProperty("tooltip", pproperties);
	widgetutils.getProperty("width", pproperties);
	SortCode_colwidth();
	widgetutils.getProperty("customwidth", pproperties);
	widgetutils.getProperty("customwidthtype", pproperties);
	widgetutils.getProperty("labelwidth", pproperties);
	widgetutils.getProperty("contentalignment", pproperties);
	widgetutils.getProperty("labelalignment", pproperties);
	widgetutils.getProperty("translatedefaultvalue", pproperties);
	widgetutils.getProperty("placeholder", pproperties);
	widgetutils.getProperty("state", pproperties);
	widgetutils.getProperty("options", pproperties);
	widgetutils.getProperty("headeralignment", pproperties);
	widgetutils.getProperty("hint", pproperties);
	widgetutils.getProperty("cssclasses", pproperties);
	widgetutils.getEventElements("eventstable", pproperties);
	widgetutils.getProperty("account", pproperties);
};

/************************************************************************
 Below function is called from layout.saveProperties() to save the widget properties
 layout.populateProperties() will be called when the widget is deselected in the designer.
 Thats when the widget's properties needs to be saved from the properties page.
 
 Send the widget's property id to the widgetutils.setPropety() function with 'pproperties' object
 So that the value will be stored in 'pproperties' object.
 Example: In following lines id and pid are passed to the widgetutils.setProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.SortCode.saveProperties = function(pproperties) {
	widgetutils.setProperty("id", pproperties);
	widgetutils.setProperty("pid", pproperties);
	widgetutils.setProperty("title", pproperties);
	widgetutils.setProperty("width", pproperties);
	widgetutils.setProperty("customwidth", pproperties);
	widgetutils.setProperty("customwidthtype", pproperties);
	widgetutils.setProperty("labelwidth", pproperties);
	widgetutils.setProperty("contentalignment", pproperties);
	widgetutils.setProperty("labelalignment", pproperties);
	widgetutils.setProperty("translatedefaultvalue", pproperties);
	widgetutils.setProperty("placeholder", pproperties);
	widgetutils.setProperty("state", pproperties);
	widgetutils.setProperty("options", pproperties);
	widgetutils.setProperty("headeralignment", pproperties);
	widgetutils.setProperty("tooltip", pproperties);
	widgetutils.setProperty("hint", pproperties);
	widgetutils.setProperty("cssclasses", pproperties);
	widgetutils.setEvent("eventstable", pproperties);
	widgetutils.setProperty("account", pproperties);
};

/************************************************************************
 Below function is called while generating the widget html from IDE
 Provide the html as you want to display the widget in the generated page.
	
 In below function lcontent contains the widget related data. Access them with .propertyId
 Ex : If there is a property called title you can access it as lcontent.title
 ************************************************************************/
function getHTML(pproperties) {
 var lcontent = JSON.parse(pproperties);
 var html = "",rowNo = "";
 var alignment = "", cssCls = "", shownoneCls = "", toolTip = "", tooltipCls = "", placeholder = "", stateAttr = "",accCls = "";
 if(lcontent.contentalignment == "LEFT"){
     alignment = "tleft";
 } else if(lcontent.contentalignment == "CENTER"){
     alignment = "tcenter";
 } else if(lcontent.contentalignment == "RIGHT"){
     alignment = "tright";
 }
 if (lcontent.state == "DISABLED") {
     stateAttr = " disabled='disabled'";
 } else if (lcontent.state == "READONLY") {
     stateAttr = " readonly='readonly'";
 }
 if (lcontent.tooltip) {
    toolTip = " original-title="+lcontent.tooltip+"";
    tooltipCls = " tooltipcls";
}
if (lcontent.placeholder) {
    placeholder = " placeholder="+lcontent.placeholder+"";
}
 if (lcontent.options == "N") {
     shownoneCls = " shownone";
 }
 if (lcontent.cssclasses != undefined) {
     cssCls = " " + lcontent.cssclasses;
 }
 if (lcontent.rowno != undefined && lcontent.rowno != -1) {
     rowNo = "_"+lcontent.rowno;
 }
 if (lcontent.account=="Y") {
     accCls = " accountreq";
 }
 if (lcontent.state == "READONLY") {
     html = '<ul id ="'+lcontent.name+rowNo+'"  widgetcategory="element" widgetid="SortCode" '+toolTip+' class="hrow sortcode '+cssCls+shownoneCls+tooltipCls+'" rowno="'+lcontent.rowno+'">'+
    	'<li><p type="text" id="'+lcontent.name+'_00'+rowNo+'" class="sort-code '+alignment+'" '+placeholder+stateAttr+' rowno="'+lcontent.rowno+'" ></p></li>'+
    	'<li class="tcenter">-</li>'+
    	'<li><p type="text" id="'+lcontent.name+'_11'+rowNo+'" class="sort-code '+alignment+'" '+placeholder+stateAttr+' rowno="'+lcontent.rowno+'" ></p></li>'+
    	'<li class="tcenter">-</li>'+
    	'<li><p type="text" id="'+lcontent.name+'_22'+rowNo+'" class="sort-code '+alignment+'" '+placeholder+stateAttr+' rowno="'+lcontent.rowno+'" ></p></li>'+
    	'<li class="tcenter">&nbsp;</li>'+
    '</ul>';
 } else {
     html = '<ul id ="'+lcontent.name+rowNo+'"  widgetcategory="element" widgetid="SortCode" '+toolTip+' class="hrow sortcode '+cssCls+shownoneCls+tooltipCls+'" rowno="'+lcontent.rowno+'">'+
    	'<li><input type="text" id="'+lcontent.name+'_00'+rowNo+'" class="sort-code apz-element '+alignment+'" '+placeholder+stateAttr+' rowno="'+lcontent.rowno+'" onkeydown="apz.SortCode.validateInput(this,event);"></li>'+
    	'<li class="tcenter">&#8211;</li>'+
    	'<li><input type="text" id="'+lcontent.name+'_11'+rowNo+'" class="sort-code apz-element '+alignment+'" '+placeholder+stateAttr+' rowno="'+lcontent.rowno+'" onkeydown="apz.SortCode.validateInput(this,event);"></li>'+
    	'<li class="tcenter">&#8211;</li>'+
    	'<li><input type="text" id="'+lcontent.name+'_22'+rowNo+'" class="sort-code apz-element '+alignment+accCls+'" '+placeholder+stateAttr+' onkeydown="apz.SortCode.validateInput(this,event);" rowno="'+lcontent.rowno+'"></li>'+
    	'<li class="tcenter">&nbsp;</li>'+
    '</ul>';
 }
 
 return html;
}

function SortCode_displayElementsProperties(pproperties) {
    var id = document.getElementById("id").value;
	var parentContainer = utils.getContainer($("#"+id)[0]).getAttribute("widgettype");
	if (parentContainer == "TABLE") {
		$(".forTable").removeClass("shownone");//css("display", "block");
	}
	if (parentContainer == "NAVBAR") {
		$(".forNavbar").removeClass("shownone");
	}
	if (parentContainer == "FORM") {
		$(".forForm").removeClass("shownone");
	}
	//// fix 1648
	if (parentContainer == "LIST") {
		$(".forList").removeClass("shownone");
	}
	if(pproperties.draggable == "Y"){
		$(".draggableid").removeClass("shownone");
	} else {
		$(".draggableid").addClass("shownone");
	}
	if(pproperties) {
		var interfacename = pproperties.interfacename;
		var datamodeltyp = pproperties.datamodeltype;
		var nodename = pproperties.nodename;
		var elementname = pproperties.elementname;
	} else {
		var interfacename = document.getElementById("interfacename").value;
		var datamodeltyp = document.getElementById("datamodeltype").value;
		var nodename = document.getElementById("nodename").value;
		var elementname = document.getElementById("elementname").value;
	}
	if(utils.isDmlObj(interfacename,datamodeltyp, nodename, elementname)) {
		$(".handleso").addClass("disabled");
	} else {
		$(".handleso").removeClass("disabled");
	}
}

function SortCode_title(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.title;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("title").value;
	}
	se = widgetutils.getLITValue(se);
	var parentContainer = utils.getContainer($("#"+res)[0]);
	var parentContainerType = parentContainer.getAttribute("widgettype");
	var parentId = $("#"+res)[0].getAttribute("parentid");
	if(parentContainerType != "TABLE") {
		var parentobject = $("#" + parentId);
		if (parentContainerType == "FORM") {
		    if(parentobject[0].hasAttribute("role")) {
    			var firstChild = parentobject.find("#"+parentId+"_receiver").children()[0].id;
    			if(firstChild == res)
    				parentobject.find("#group1").html("");
    		} else {
    		    $("#" + res).find("#label").html(se);
    		}
		}
	} else {
		if(utils.isNull(se)) {
       		$("#"+res+"_heading").html("&nbsp;");
		} else {
			$("#"+res+"_heading").html(se);	
		}
	}
}

function SortCode_placeholder(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.placeholder;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("placeholder").value;
	}
	se = widgetutils.getLITValue(se);
	if(!se){
		se = "";
	}
	var temp = $("#" + res).find(".sortcode #sortcodeinput");
	$(temp).attr('placeholder', se);
}

function SortCode_labelwidth(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelwidth;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelwidth").value;
	}
	if(!utils.isNull(se) && $.isNumeric(se)){
		var ae = 100 - se;
	}else{
		var ae = 100;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
			var tem = $("#" + res).find("#content");
			if(!utils.isNull(se)) {
				$(temp).removeClass(classarray.join(' ')).addClass('w' + se);
				$(tem).removeClass(classarray.join(' ')).addClass('w' + ae);
			} else {
				$(temp).removeClass(classarray.join(' ')).addClass('w40');
				$(tem).removeClass(classarray.join(' ')).addClass('w60');
			}
		}
	}
}

function SortCode_colwidth(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.width;
		var custom = pobject.customwidth;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("width").value;
		var custom = document.getElementById("customwidth");
	}
	var temp = $("#" + res).find(".sortcode #sortcodeinput");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if((!utils.isNull(se)) && se != "CUSTOM") {
		$(custom).attr('disabled', 'disabled');
		$("#customwidthtype").attr('disabled', 'disabled');
		$(custom).val("");
		$(temp).removeClass(classarray.join(' '));
		$(temp).css('width', '');
		$(temp).addClass('w' + se);
	} else {
		$(custom).removeAttr('disabled');
		$("#customwidthtype").removeAttr('disabled');
	}
}

function SortCode_customwidth(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.customwidth;
		var setype = pobject.customwidthtype;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("customwidth").value;
		var setype = document.getElementById("customwidthtype").value;
	}
	if(!utils.isNull(se)) {
		var temp = $("#" + res).find(".sortcode #sortcodeinput");
		$(temp).css('width', se+setype);
	}
}

function SortCode_conallign(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.contentalignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("contentalignment").value;
	}
	var temp = $("#" + res).find(".sortcode #sortcodeinput");
	if(se == "LEFT")
		$(temp).removeClass('tright tcenter').addClass('tleft');
	else if(se == "RIGHT")
		$(temp).removeClass('tleft tcenter').addClass('tright');
	else if(se == "CENTER")
		$(temp).removeClass('tright tleft').addClass('tcenter');
}

function SortCode_labelallign(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelalignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelalignment").value;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
        	if(se == "LEFT")
        		$(temp).removeClass('tright tcenter').addClass('tleft');
        	else if(se == "RIGHT")
        		$(temp).removeClass('tleft tcenter').addClass('tright');
        	else if(se == "CENTER")
        		$(temp).removeClass('tright tleft').addClass('tcenter');
        	else
        		$(temp).removeClass('tright tcenter tleft');
		}
	}
}

function SortCode_state(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.state;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("state").value;
	}
	var temp = $("#" + res).find(".sortcode #sortcodeinput");
	if(se == "DISABLED") {
		$(temp).attr('disabled', 'disabled');
		$(temp)[0].type = 'text';
	} else if(se == "READONLY") {
		$(temp).removeAttr('disabled');
		$(temp).attr('readonly', 'true');
		$(temp)[0].type = 'text';
	} else {
		$(temp).removeAttr('disabled');
		$(temp).removeAttr('readonly');
		$(temp)[0].type = 'text';
	}
}

function SortCode_headerAlign(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.headeralignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("headeralignment").value;
	}
	var index = $("#" + res).parent("tr").children().index($("#"+res));
	var temp = $("#"+($("#"+res).closest("tbody").attr("parentid")+"_header")).children()[index];
	if(se == "LEFT")
		$(temp).removeClass('tright tcenter').addClass('tleft');
	else if(se == "RIGHT")
		$(temp).removeClass('tleft tcenter').addClass('tright');
	else 
		$(temp).removeClass('tright tleft').addClass('tcenter');
}

/************************************************************************
 Below function is called from layout.applyProperties() to apply the widget properties after loading the widget in designer.
 Call the functions inside below function to apply the properties (like title) to the widget.
 Pass the 'pobject' as the parameter to the functions as pobject containes the data related to that widget.
 ************************************************************************/
customwidgets.SortCode.applyProperties = function(pobject) {
    var parentContainer = utils.getContainer($("#"+pobject.id)[0]).getAttribute("widgettype");
	SortCode_title(pobject);
	SortCode_colwidth(pobject);
	SortCode_customwidth(pobject);
	SortCode_conallign(pobject);
	SortCode_state(pobject);
	SortCode_placeholder(pobject);
	widgetutils.setWidgetVisibility(pobject);
	if(parentContainer == "FORM") {
		SortCode_labelwidth(pobject);
	    SortCode_labelallign(pobject);
	}
	if(parentContainer == "TABLE") {
	    SortCode_headerAlign(pobject);
	}
};
