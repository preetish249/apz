/************************************************************************
 Below are the widget related objects initializations needs to be provided at the time of loding the js.
 ************************************************************************/
if (customwidgets == null || customwidgets == "undefined") {
    var customwidgets = {};
}
customwidgets.DropDownasButton = {};
customwidgets.DropDownasButton.currobj = null;
/************************************************************************
 Below function is called from layout.loadPropPanel() to initialize the widget related functions.
 Functions initializations are done here.
 Functions here are called on certain events like onblur or onchange of the widget properties.
 Consider the widget have a property as Title with id as title
 Then the initialization looks like this :
 $("#title").blur(function() {
 changeTitle();
 });
 Provided the function changeTitle() in the js which changes the title of the widget
 ************************************************************************/
customwidgets.DropDownasButton.init = function(pobject) {
    customwidgets.DropDownasButton.currobj = pobject;
    var currobjary = designer.selectedobjs;
    var currobjspntr = new Array();
    for (var i = 0; i < currobjary.length; i++) {
        currobjspntr[i] = utils.getDataPointer(currobjary[i]);
    }
    //Apz312 changes start
    $("#mainPropCollapse").click(function() {
        $("#mainPropAccordian").toggle();
    });
    $("#additionalpropcollapse").click(function() {
        $("#additionalprop").toggle();
    });
    $("#eventscollapse").click(function() {
        $("#eventsaccordian").toggle();
    });
    $("#title").blur(function() {
        DropDownasButton_title();
    });
    $("#width").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("width", ldatapointer);
                try {
                    DropDownasButton_colwidth(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_colwidth();
        }
    });
    $("#customwidth").blur(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("customwidth", ldatapointer);
                try {
                    DropDownasButton_customwidth(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_customwidth();
        }
    });
    $("#customwidthtype").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("customwidthtype", ldatapointer);
                try {
                    DropDownasButton_customwidth(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_customwidth();
        }
    });
    $("#labelwidth").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("labelwidth", ldatapointer);
                try {
                    DropDownasButton_labelwidth(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_labelwidth();
        }
    });
    $("#cssclasses").blur(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("cssclasses", ldatapointer);
            }
        }
    });
    $("#contentalignment").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("contentalignment", ldatapointer);
                try {
                    DropDownasButton_conallign(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_conallign();
        }
    });
    $("#labelalignment").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("labelalignment", ldatapointer);
                try {
                    DropDownasButton_labelallign(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_labelallign();
        }
    });
    $("#state").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("state", ldatapointer);
                try {
                    DropDownasButton_state(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_state();
        }
    });
    $("#semantics").change(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("semantics", ldatapointer);
                try {
                    DropDownasButton_semantics(ldatapointer);
                } catch (e) {}
            }
        }
        else{
            DropDownasButton_semantics();
        }
    });
    $("#defaultvalue").change(function() {
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("defaultvalue", ldatapointer);
                try {
                    DropDownasButton_defaultvalue(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_defaultvalue();
        }
    });
    $("#options").change(function() {
        var lval = $("#options").prop("checked") ? "Y" : "N";
        $("#options")[0].value = lval;
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("options", ldatapointer);
                try {
                    widgetutils.setWidgetVisibility(ldatapointer);
                } catch (e) {}
            }
        } else {
            widgetutils.setWidgetVisibility();
        }
    });
    //Apz312 changes end
    $("#selectall").click(function() {
        widgetutils.setMultipleCheckboxes(this, "selectevent");
    });
    $("#headeralignment").change(function() {
        var lval = $("#headeralignment").prop("checked") ? "Y" : "N";
        $("#headeralignment")[0].value = lval;
        if (designer.multipleselect) {
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("headeralignment", ldatapointer);
                try {
                    DropDownasButton_headerAlign(ldatapointer);
                } catch (e) {}
            }
        } else {
            DropDownasButton_headerAlign();
        }
    });
};
/************************************************************************
 Below function is called from layout.addWidget() to get the widget template provided by the developer.
 Provided widgethtml content will be displayed inside the Designer screen.
 Keep the provided outer div (with id appzillonid) as it contains widget related initializations, and keep your content inside that div.
 ************************************************************************/
customwidgets.DropDownasButton.getTemplate = function(pobject, pparentobject,receviedForChild,uiParentObj) {
    var widgethtml = "";
    var parentContainer = utils.getContainer(pparentobject).getAttribute("widgettype");
    var parentContId = utils.getContainer(pparentobject).id;
    if(parentContainer == "TABLE") {
		var header = $("#"+parentContId).find("#"+parentContId+"_header");
		var newHeader = "<th onclick='widgetutils.selectTableElement(this, event);'>Dropdown As Button</th>";
		var parentContainer = utils.getContainer(pparentobject);
		if(receviedForChild) {
			var $uiParentObj = $(uiParentObj);
			var parentIndex = $uiParentObj.parent().children().index(uiParentObj);
			var uiParentHeader = header.children()[parentIndex];
			$(uiParentHeader).before(newHeader);
		} else{
			header.append(newHeader);
		}
		widgethtml = "<td id ='appzillonid' class='apzlocontainer'>";
		var elmContent = '<div class="eboxh"><div class="button-group"><button class="ebtn with-dropddown">&nbsp;</button><div class="dropdown-list"><ul></ul></div></div></div>';
		widgethtml = widgethtml + elmContent;
		widgethtml = widgethtml + "</td>";
    } else if(parentContainer == "LIST" || parentContainer == "NAVBAR") {
        widgethtml = '<div class="eobox" id ="appzillonid"  widgetcategory="elements" widgetid="DropdownAsButton" tabindex="0"><div class="button-group"><button class="ebtn with-dropddown">Select</button><div class="dropdown-list"><ul></ul></div></div></div>';
    } else {
        if(pparentobject.hasAttribute("role")) {
            var receivedTemplate = '<div class="button-group"><button class="ebtn with-dropddown">Select</button><div class="dropdown-list"><ul></ul></div></div>';
			widgethtml = '<li id ="appzillonid" class="apzlocontainer elpad">';
			widgethtml = widgethtml + receivedTemplate;
			widgethtml = widgethtml + '</li>';
        } else {
            widgethtml = '<ul id ="appzillonid" class="apzlocontainer srow eobox wrapped"><li id="label" class="w40"></li><li id="content" class="w60 ebox"><div class="button-group"><button class="ebtn with-dropddown">Select</button><div class="dropdown-list"><ul></ul></div></div></li></ul>';
        }
    }
	return widgethtml;
};
/************************************************************************
 Below function is called from layout.populateProperties() to populate the widget properties provided by the developer
 layout.populateProperties() will be called when the widget has been selected in the designer.
 Thats when the widget properties page will be populated.
	
 Send the widgets property id to the widgetutils.getPropety() function with 'pproperties' object
 So that the value stored in 'pproperties' object will be assigned to the element in the screen with the id which has been passed.
 Example: In below function id and pid are passed to the widgetutils.getProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.DropDownasButton.populateProperties = function(pproperties) {
    widgetutils.getProperty("id", pproperties);
    widgetutils.getProperty("pid", pproperties);
    widgetutils.getProperty("title", pproperties);
    DropDownasButton_displayElementsProperties(pproperties);
    widgetutils.getProperty("tooltip", pproperties);
    widgetutils.getProperty("width", pproperties);
    DropDownasButton_colwidth();
    widgetutils.getProperty("customwidth", pproperties);
    widgetutils.getProperty("customwidthtype", pproperties);
    widgetutils.getProperty("labelwidth", pproperties);
    widgetutils.getProperty("contentalignment", pproperties);
    widgetutils.getProperty("labelalignment", pproperties);
    widgetutils.getProperty("translatedefaultvalue", pproperties);
    widgetutils.getProperty("placeholder", pproperties);
    widgetutils.getProperty("state", pproperties);
    widgetutils.getProperty("semantics", pproperties);
    widgetutils.getProperty("options", pproperties);
    widgetutils.getProperty("hint", pproperties);
    widgetutils.getProperty("cssclasses", pproperties);
    widgetutils.getEventElements("eventstable", pproperties);
    DropDownasButton_displayElmRelatedProps();
    DropDownasButton_getDropdowns("staticoptions", pproperties);
    DropDownasButton_defaultvalue(pproperties);
};
/************************************************************************
 Below function is called from layout.saveProperties() to save the widget properties
 layout.populateProperties() will be called when the widget is deselected in the designer.
 Thats when the widget's properties needs to be saved from the properties page.
 
 Send the widget's property id to the widgetutils.setPropety() function with 'pproperties' object
 So that the value will be stored in 'pproperties' object.
 Example: In following lines id and pid are passed to the widgetutils.setProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.DropDownasButton.saveProperties = function(pproperties) {
    widgetutils.setProperty("id", pproperties);
    widgetutils.setProperty("pid", pproperties);
    widgetutils.setProperty("title", pproperties);
    widgetutils.setProperty("width", pproperties);
    widgetutils.setProperty("customwidth", pproperties);
    widgetutils.setProperty("customwidthtype", pproperties);
    widgetutils.setProperty("labelwidth", pproperties);
    widgetutils.setProperty("contentalignment", pproperties);
    widgetutils.setProperty("labelalignment", pproperties);
    widgetutils.setProperty("translatedefaultvalue", pproperties);
    widgetutils.setProperty("placeholder", pproperties);
    widgetutils.setProperty("state", pproperties);
    widgetutils.setProperty("semantics", pproperties);
    widgetutils.setProperty("options", pproperties);
    widgetutils.setProperty("tooltip", pproperties);
    widgetutils.setProperty("hint", pproperties);
    widgetutils.setProperty("cssclasses", pproperties);
    widgetutils.setEvent("eventstable", pproperties);
    DropDownasButton_displayElmRelatedProps();
    DropDownasButton_setDropdowns("staticoptions",pproperties);
};
/************************************************************************
 Below function is called while generating the widget html from IDE
 Provide the html as you want to display the widget in the generated page.
	
 In below function lcontent contains the widget related data. Access them with .propertyId
 Ex : If there is a property called title you can access it as lcontent.title
 ************************************************************************/
function getHTML(pproperties) {
    var lcontent = JSON.parse(pproperties);
    var alignment = "", toolTip = "", tooltipCls = "", cssCls = "", shownoneCls = "";
    var stateAttr = "", defVal = "&nbsp;";
    var statOpts = lcontent.staticoptions;
    if (lcontent.contentalignment == "LEFT") {
        alignment = " tleft";
    } else if (lcontent.contentalignment == "CENTER") {
        alignment = " tcenter";
    } else if (lcontent.contentalignment == "RIGHT") {
        alignment = " tright";
    }
    var state = lcontent.state;
    if (state=='DISABLED') {
        stateAttr = ' disabled="disabled"';
    }
    if (lcontent.tooltip) {
        toolTip = " original-title="+lcontent.tooltip+"";
        tooltipCls = " tooltipcls";
    }
    var semantics = "";
    if(lcontent.semantics){
     semantics = semantics.toLowerCase();
    }
    if (lcontent.defaultvalue) {
        defVal = lcontent.defaultvalue;
    }
    if (lcontent.options == "N") {
         shownoneCls = " shownone";
     }
     if (lcontent.cssclasses != undefined) {
         cssCls = " " + lcontent.cssclasses;
     }
    var html = '<div id = "'+lcontent.name+'_ext" '+toolTip+' class="'+alignment+tooltipCls+cssCls+shownoneCls+' button-group w'+lcontent.width+' "><button id="'+lcontent.name+'" class="ebtn with-dropddown '+semantics+alignment+'" '+stateAttr+'>'+defVal+'</button><div id ="' + lcontent.name +'_div" class="dropdown-list"><ul role="menu">';
        if(statOpts && statOpts.length > 0) {
        	for(i=0;i< statOpts.length;i++) {
        		html += '<li><a href = "'+lcontent.staticoptions[i].value+'" target= "_blank">'+lcontent.staticoptions[i].description+'</a></li>';
        	}
        }
        html += '</ul></div></div>';
    return html;
}

function DropDownasButton_displayElementsProperties(pproperties) {
    var id = document.getElementById("id").value;
	var parentContainer = utils.getContainer($("#"+id)[0]).getAttribute("widgettype");
	if (parentContainer == "TABLE") {
		$(".forTable").removeClass("shownone");//css("display", "block");
	}
	if (parentContainer == "NAVBAR") {
		$(".forNavbar").removeClass("shownone");
	}
	if (parentContainer == "FORM") {
		$(".forForm").removeClass("shownone");
	}
	//// fix 1648
	if (parentContainer == "LIST") {
		$(".forList").removeClass("shownone");
	}
	if(pproperties.draggable == "Y"){
		$(".draggableid").removeClass("shownone");
	} else {
		$(".draggableid").addClass("shownone");
	}
	if(pproperties) {
		var interfacename = pproperties.interfacename;
		var datamodeltyp = pproperties.datamodeltype;
		var nodename = pproperties.nodename;
		var elementname = pproperties.elementname;
	} else {
		var interfacename = document.getElementById("interfacename").value;
		var datamodeltyp = document.getElementById("datamodeltype").value;
		var nodename = document.getElementById("nodename").value;
		var elementname = document.getElementById("elementname").value;
	}
	if(utils.isDmlObj(interfacename,datamodeltyp, nodename, elementname)) {
		$(".handleso").addClass("disabled");
	} else {
		$(".handleso").removeClass("disabled");
	}
}

function DropDownasButton_title(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.title;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("title").value;
	}
	se = widgetutils.getLITValue(se);
	var parentContainer = utils.getContainer($("#"+res)[0]);
	var parentContainerType = parentContainer.getAttribute("widgettype");
	var parentId = $("#"+res)[0].getAttribute("parentid");
	if(parentContainerType == "TABLE") {
		if(utils.isNull(se)) {
       		$("#"+res+"_heading").html("&nbsp;");
		} else {
			$("#"+res+"_heading").html(se);	
		}
	}
}

function DropDownasButton_labelwidth(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelwidth;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelwidth").value;
	}
	if(!utils.isNull(se) && $.isNumeric(se)){
		var ae = 100 - se;
	}else{
		var ae = 100;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
			var tem = $("#" + res).find("#content");
			if(!utils.isNull(se)) {
				$(temp).removeClass(classarray.join(' ')).addClass('w' + se);
				$(tem).removeClass(classarray.join(' ')).addClass('w' + ae);
			} else {
				$(temp).removeClass(classarray.join(' ')).addClass('w40');
				$(tem).removeClass(classarray.join(' ')).addClass('w60');
			}
		}
	}
}

function DropDownasButton_colwidth(pobject) {
    if (pobject != undefined) {
        var res = pobject.id;
        var se = pobject.width;
        var custom = pobject.customwidth;
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("width").value;
        var custom = document.getElementById("customwidth");
    }
    var temp = $("#" + res).find("div.button-group");
    var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
    if ((!utils.isNull(se)) && se != "CUSTOM") {
        $(custom).attr('disabled', 'disabled');
        $("#customwidthtype").attr('disabled', 'disabled');
        $(custom).val("");
        $(temp).removeClass(classarray.join(' '));
        $(temp).css('width', '');
        $(temp).addClass('w' + se);
    } else {
        $(custom).removeAttr('disabled');
        $("#customwidthtype").removeAttr('disabled');
    }
}

function DropDownasButton_customwidth(pobject) {
    if (pobject != undefined) {
        var res = pobject.id;
        var se = pobject.customwidth;
        var setype = pobject.customwidthtype;
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("customwidth").value;
        var setype = document.getElementById("customwidthtype").value;
    }
    if (!utils.isNull(se)) {
        var temp = $("#" + res).find(".with-dropddown #dropdownbtn");
        $(temp).css('width', se + setype);
    }
}

function DropDownasButton_conallign(pobject) {
    if (pobject != undefined) {
        var res = pobject.id;
        var se = pobject.contentalignment;
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("contentalignment").value;
    }
    var temp = $("#" + res).find(".dropdown-list");
    var tempButton = $("#" + res).find(".with-dropddown");
    if (se == "LEFT") {
        $(temp).removeClass('tright tcenter').addClass('tleft');
        $(tempButton).removeClass('tright tcenter').addClass('tleft');  
    } else if (se == "RIGHT") {
        $(temp).removeClass('tleft tcenter').addClass('tright');
        $(tempButton).removeClass('tleft tcenter').addClass('tright');
    } else if (se == "CENTER") {
        $(temp).removeClass('tright tleft').addClass('tcenter');
        $(tempButton).removeClass('tright tleft').addClass('tcenter');
    }
}

function DropDownasButton_labelallign(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelalignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelalignment").value;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
        	if(se == "LEFT")
        		$(temp).removeClass('tright tcenter').addClass('tleft');
        	else if(se == "RIGHT")
        		$(temp).removeClass('tleft tcenter').addClass('tright');
        	else if(se == "CENTER")
        		$(temp).removeClass('tright tleft').addClass('tcenter');
        	else
        		$(temp).removeClass('tright tcenter tleft');
		}
	}
}

function DropDownasButton_state(pobject) {
    if (pobject != undefined) {
        var res = pobject.id;
        var se = pobject.state;
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("state").value;
    }
    var temp = $("#" + res).find('button');
    if (se == "DISABLED") {
        $(temp).addClass('disabled');
    } else {
        $(temp).removeClass('disabled');
    }
}

function DropDownasButton_semantics(pobject) {
    if(pobject != undefined) {
        var res = pobject.id;
        var se = pobject.semantics;
    } else {
        var se = document.getElementById("semantics").value;
        var res = document.getElementById("id").value;
    }
    var temp = $("#" + res).find('button');
    var classarray = ['ebtn-primary', 'ebtn-info', 'ebtn-warning', 'ebtn-danger', 'ebtn-success', 'ebtn-inverse'];
    $(temp).removeClass(classarray.join(' '));
    if(!utils.isNull(se))
        $(temp).addClass(se.toLowerCase());
}

function DropDownasButton_headerAlign(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.headeralignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("headeralignment").value;
	}
	var index = $("#" + res).parent("tr").children().index($("#"+res));
	var temp = $("#"+($("#"+res).closest("tbody").attr("parentid")+"_header")).children()[index];
	if(se == "LEFT")
		$(temp).removeClass('tright tcenter').addClass('tleft');
	else if(se == "RIGHT")
		$(temp).removeClass('tleft tcenter').addClass('tright');
	else 
		$(temp).removeClass('tright tleft').addClass('tcenter');
}

function DropDownasButton_displayElmRelatedProps () {
    widgetutils.updatePropertyVisibility("translatedefaultvalue","NO");
    widgetutils.updatePropertyVisibility("minstringlength","NO");
    widgetutils.updatePropertyVisibility("maxstringlength","NO");
    widgetutils.updatePropertyVisibility("minvalue","NO");
    widgetutils.updatePropertyVisibility("maxvalue","NO");
    widgetutils.updatePropertyVisibility("pattern","NO");
    widgetutils.updatePropertyVisibility("mandatory","NO");
}

function DropDownasButton_getDropdowns(ptable, pproperties) {
	if( typeof pproperties.staticoptions != 'undefined') {
		$("#" + ptable + " tbody").empty();
		for( i = 1; i <= pproperties.staticoptions.length; i++) {
			form_dropdown_addDropdown(pproperties.staticoptions[i - 1]);
			$("#" + ptable + " tr:eq(" + i + ") #value").val(pproperties.staticoptions[i - 1]["value"]);
			$("#" + ptable + " tr:eq(" + i + ") #description").val(pproperties.staticoptions[i - 1]["description"]);
			if(pproperties.staticoptions[i - 1]["group"] == "Y")
				$("#" + ptable + " tr:eq(" + i + ") #group").prop("checked",true);
		}
	}
}

function DropDownasButton_setDropdowns(ptable, pproperties) {
	var rows = $("#" + ptable + " tr:gt(0)");
	var i = 1;
	pproperties.staticoptions = [];
	rows.each(function(index) {
		if(!pproperties.staticoptions[i])
			pproperties.staticoptions[i - 1] = {
			};
		pproperties.staticoptions[i - 1]["description"] = $("#" + ptable + " tr:eq(" + i + ") #description").val();
		pproperties.staticoptions[i - 1]["value"] = $("#" + ptable + " tr:eq(" + i + ") #value").val();
		pproperties.staticoptions[i - 1]["group"] = "N";
		if($("#" + ptable + " tr:eq(" + i + ") #group").prop("checked"))
			pproperties.staticoptions[i - 1]["group"] = "Y";
		i += 1;
	});
}

DropDownasButton_defaultvalue = function (pobject) {
    var res, defaultvalue;
    if(pobject) {
        res = pobject.id;
        defaultvalue = pobject.defaultvalue;
    } else {
        res = document.getElementById("id").value;
        defaultvalue = document.getElementById("defaultvalue").value;
    }
    $("#"+res).find('button').html(defaultvalue);
}
/************************************************************************
 Below function is called from layout.applyProperties() to apply the widget properties after loading the widget in designer.
 Call the functions inside below function to apply the properties (like title) to the widget.
 Pass the 'pobject' as the parameter to the functions as pobject containes the data related to that widget.
 ************************************************************************/
customwidgets.DropDownasButton.applyProperties = function(pobject) {
    var parentContainer = utils.getContainer($("#"+pobject.id)[0]).getAttribute("widgettype");
    DropDownasButton_colwidth(pobject);
    DropDownasButton_customwidth(pobject);
    DropDownasButton_conallign(pobject);
    DropDownasButton_state(pobject);
    widgetutils.setWidgetVisibility(pobject);
    DropDownasButton_defaultvalue(pobject);
    DropDownasButton_semantics(pobject);
    if (parentContainer == "TABLE") {
        DropDownasButton_title(pobject);
        DropDownasButton_headerAlign(pobject);
    } else if (parentContainer == "FORM") {
        DropDownasButton_labelwidth(pobject);
        DropDownasButton_labelallign(pobject);
    }
};
