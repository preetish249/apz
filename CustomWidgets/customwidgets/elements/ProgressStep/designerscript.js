/************************************************************************
 Below are the widget related objects initializations needs to be provided at the time of loding the js.
 ************************************************************************/
if (customwidgets == null || customwidgets == "undefined") {
 var customwidgets = {};
}
customwidgets.ProgressStep = {};
customwidgets.ProgressStep.currobj = null;

/************************************************************************
 Below function is called from layout.loadPropPanel() to initialize the widget related functions.
 Functions initializations are done here.
 Functions here are called on certain events like onblur or onchange of the widget properties.
 Consider the widget have a property as Title with id as title
 Then the initialization looks like this :
 $("#title").blur(function() {
 changeTitle();
 });
 Provided the function changeTitle() in the js which changes the title of the widget
 ************************************************************************/
customwidgets.ProgressStep.init = function(pobject) {
    customwidgets.ProgressStep.currobj = pobject;
    var currobjary = designer.selectedobjs;
    var currobjspntr = new Array();
    for (var i = 0; i < currobjary.length; i++) {
        currobjspntr[i] = utils.getDataPointer(currobjary[i]);
    }
    $("#title").blur(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("title", ldatapointer);
                try {
                    ProgressStep_title(ldatapointer);
                } catch (e) {}
            }
        }
        else{
            ProgressStep_title();
        }
    });
    $("#controlid").blur(function() {
                for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("controlid", ldatapointer);
    }
    });
    //Apz312 changes start
    $("#mainPropCollapse").click(function(){
        $("#mainPropAccordian").toggle();
    });
    $("#addcollapse").click(function(){
        $("#addaccordian").toggle();
    });
    $("#eventscollapse").click(function(){
        $("#eventsaccordian").toggle();
    });
    //Apz312 changes end
    $("#options").change(function() {
        var lval = $("#options").prop("checked") ? "Y" : "N";
        $("#options")[0].value = lval;
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("options", ldatapointer);
                try {
                    //form_filebrowser_tablecellminwidth(ldatapointer);
                    widgetutils.setWidgetVisibility(ldatapointer);
                } catch (e) {}
            }
        }
        else{
        widgetutils.setWidgetVisibility();
        }
    });
    $("#menu").blur(function () {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("menu", ldatapointer);
                try {
                    ProgressStep_menu(ldatapointer);
                } catch (e) {}
            }
        }
        else{
        ProgressStep_menu();
        }
    });
    $("#headeralignment").change(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("headeralignment", ldatapointer);
                try {
                    ProgressStep_headeralignment(ldatapointer);
                } catch (e) {}
            }
        }
        else{
        ProgressStep_headeralignment();
        }
    });
    $("#columnalignment").change(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("columnalignment", ldatapointer);
            }
        }
    });
    $("#labelalignment").change(function() {
		if(designer.multipleselect){
			for (var i = 0; i < currobjary.length; i++) {
				$("#id").val(currobjary[i].id);
				var ldatapointer = currobjspntr[i];
				widgetutils.setProperty("labelalignment", ldatapointer);
				try {
					ProgressStep_labelallign(ldatapointer);
				} catch (e) {}
			}
		} else{
		    ProgressStep_labelallign();
		}
	});
    $("#labelwidth").change(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("labelwidth", ldatapointer);
                try {
                    //form_filebrowser_tablecellminwidth(ldatapointer);
                    ProgressStep_labelwidth(ldatapointer);
                } catch (e) {}
            }
        } else{
            ProgressStep_labelwidth();
        }
    });
    $("#defaultvalue").blur(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("defaultvalue", ldatapointer);
                try {
                    ProgressStep_defaultvalue(ldatapointer);
                } catch (e) {}
            }
        }
        else{
        ProgressStep_defaultvalue();
        }
    });
    $("#url").blur(function() {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                $("#id").val(currobjary[i].id);
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("url", ldatapointer);
                try {
                    ProgressStep_url(ldatapointer);
                } catch (e) {}
            }
        } else{
            ProgressStep_url();
        }
    });
    $("#translatedefaultvalue").change(function () {
        $("#translatedefaultvalue")[0].value = $("#translatedefaultvalue").prop("checked") ? "Y" : "N";
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("translatedefaultvalue", ldatapointer);
                try {
                    ProgressStep_defaultvalue(ldatapointer);
                } catch (e) {}
            }
        } else{
            ProgressStep_defaultvalue();
        }
    });
    $("#cssclasses").blur(function () {
        if(designer.multipleselect){
            for (var i = 0; i < currobjary.length; i++) {
                var ldatapointer = currobjspntr[i];
                widgetutils.setProperty("cssclasses", ldatapointer);
            }
        }
    });
    $("#selectall").click(function() {
        widgetutils.setMultipleCheckboxes(this, "selectevent");
    });
    //Apz312 changes start
    $("#interfacename").change(function() {
        var linterfaceName = document.getElementById('interfacename').value;
        widgetutils.loadNodetypes("datamodeltype",linterfaceName);
        var ldatamodeltype = document.getElementById('datamodeltype').value;
        widgetutils.loadNodeNames("nodename",ldatamodeltype);
        widgetutils.resetDataModalProps(linterfaceName, ldatamodeltype, document.getElementById('nodename').value, document.getElementById('elementname').value);
        designer.interfacename = linterfaceName;
    });
    $("#datamodeltype").change(function() {
        var ldatamodeltype = document.getElementById('datamodeltype').value;
        widgetutils.loadNodeNames("nodename",ldatamodeltype)
        widgetutils.resetDataModalProps(linterfaceName, ldatamodeltype, document.getElementById('nodename').value, document.getElementById('elementname').value);
        designer.interfacename = linterfaceName;
        designer.datamodeltype = ldatamodeltype;

     });
    $("#nodename").change(function() {
        var linterfaceName = document.getElementById('interfacename').value;
        var ldatamodeltype = document.getElementById('datamodeltype').value;
        var lnodename = document.getElementById('nodename').value;
        widgetutils.loadElementNames("elementname", lnodename);
        widgetutils.resetDataModalProps(linterfaceName, ldatamodeltype, lnodename, document.getElementById('elementname').value);
        designer.interfacename = linterfaceName;
        designer.datamodeltype = ldatamodeltype;
        designer.nodename = lnodename;
    });
    $("#elementname").change(function() {
        var linterfaceName = document.getElementById('interfacename').value;
        var ldatamodeltype = document.getElementById('datamodeltype').value;
        var lnodename = document.getElementById('nodename').value;
        var lelmvalue = document.getElementById('elementname').value;
        widgetutils.resetDataModalProps(linterfaceName, ldatamodeltype, lnodename, lelmvalue);
        if(widgetutils.isElementUsed(linterfaceName, ldatamodeltype, lnodename, lelmvalue)){
            return false;
        }
        widgetutils.loadName(linterfaceName, ldatamodeltype, lnodename, lelmvalue);
        widgetutils.setElementProperties(linterfaceName, ldatamodeltype, lnodename, lelmvalue);
        designer.interfacename = linterfaceName;
        designer.datamodeltype = ldatamodeltype;
        designer.nodename = lnodename;
        designer.elementname = lelmvalue;
    });
    $("#progresstype").change(function(){
        ProgressStep_progressType();
    });

    //Apz312 changes end
};

/************************************************************************
 Below function is called from layout.addWidget() to get the widget template provided by the developer.
 Provided widgethtml content will be displayed inside the Designer screen.
 Keep the provided outer div (with id appzillonid) as it contains widget related initializations, and keep your content inside that div.
 ************************************************************************/
customwidgets.ProgressStep.getTemplate = function(pobject, pparentobject,receviedForChild,uiParentObj) {
	var widgethtml = "";
    var parentContainer = utils.getContainer(pparentobject).getAttribute("widgettype");
    var parentContId = utils.getContainer(pparentobject).id;
    if(parentContainer == "TABLE") {
		var header = $("#"+parentContId).find("#"+parentContId+"_header");
		var newHeader = "<th onclick='widgetutils.selectTableElement(this, event);'>Progress Step</th>";
		var parentContainer = utils.getContainer(pparentobject);
		if(receviedForChild) {
			var $uiParentObj = $(uiParentObj);
			var parentIndex = $uiParentObj.parent().children().index(uiParentObj);
			var uiParentHeader = header.children()[parentIndex];
			$(uiParentHeader).before(newHeader);
		} else{
			header.append(newHeader);
		}
		widgethtml = "<td id ='appzillonid' class='apzlocontainer'>";
		var elmContent = "<ul class='progress-box'><li id='prgid' class='progress-step'><span>1 Done step</span></li></ul>";
		widgethtml = widgethtml + elmContent;
		widgethtml = widgethtml + "</td>";
    } else if(parentContainer == "LIST" || parentContainer == "NAVBAR") {
        widgethtml = "<div class='eobox' id ='appzillonid'  widgetcategory='elements' widgetid='ProgressStep' tabindex='0' ><ul class='progress-box'><li id='prgid' class='progress-step'><span>1 Done step</span></li></ul></div>";
    } else {
        if(pparentobject.hasAttribute("role")) {
            var receivedTemplate = "<ul class='progress-box'><li id='prgid' class='progress-step'><span>1 Done step</span></li></ul>";
			widgethtml = '<li id ="appzillonid" class="apzlocontainer elpad">';
			widgethtml = widgethtml + receivedTemplate;
			widgethtml = widgethtml + '</li>';
        } else {
            widgethtml = "<div class='eobox' id ='appzillonid'  widgetcategory='elements' widgetid='ProgressStep' tabindex='0' ><ul class='srow'><li id='label' class='w40'>Progress Step</li><li id='content' class='w60'><ul class='progress-box'><li id='prgid' class='progress-step'><span>1 Done step</span></li></ul></li></ul></div>";
        }
    }
    return widgethtml;
};

/************************************************************************
 Below function is called from layout.populateProperties() to populate the widget properties provided by the developer
 layout.populateProperties() will be called when the widget has been selected in the designer.
 Thats when the widget properties page will be populated.
    
 Send the widgets property id to the widgetutils.getPropety() function with 'pproperties' object
 So that the value stored in 'pproperties' object will be assigned to the element in the screen with the id which has been passed.
 Example: In below function id and pid are passed to the widgetutils.getProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.ProgressStep.populateProperties = function(pproperties) {

    widgetutils.getProperty("id", pproperties);
    widgetutils.getProperty("pid", pproperties);
    widgetutils.getProperty("title", pproperties);
    widgetutils.getProperty("controlid", pproperties);
    ProgressStep_displayElementsProperties(pproperties);
    widgetutils.getProperty("headeralignment", pproperties);
    widgetutils.getProperty("columnalignment", pproperties);
    widgetutils.getProperty("options", pproperties);
    widgetutils.getProperty("labelwidth", pproperties);
    widgetutils.getProperty("labelalignment", pproperties);
    widgetutils.getProperty("hint", pproperties);
    widgetutils.getProperty("cssclasses", pproperties);
    widgetutils.getProperty("tooltip", pproperties);
    widgetutils.getProperty("translatedefaultvalue", pproperties);
    widgetutils.getProperty("progresstype", pproperties);
    widgetutils.getEventElements("eventstable", pproperties);
    ProgressStep_displayContainerRelatedProps();
};

/************************************************************************
 Below function is called from layout.saveProperties() to save the widget properties
 layout.populateProperties() will be called when the widget is deselected in the designer.
 Thats when the widget's properties needs to be saved from the properties page.
 
 Send the widget's property id to the widgetutils.setPropety() function with 'pproperties' object
 So that the value will be stored in 'pproperties' object.
 Example: In following lines id and pid are passed to the widgetutils.setProperty() function with 'pproperties'.
 ************************************************************************/
customwidgets.ProgressStep.saveProperties = function(pproperties) {
    
    widgetutils.setProperty("id", pproperties);
    widgetutils.setProperty("pid", pproperties);
    widgetutils.setProperty("title", pproperties);
    widgetutils.setProperty("controlid", pproperties);
    widgetutils.setProperty("headeralignment", pproperties);
    widgetutils.setProperty("columnalignment", pproperties);
    widgetutils.setProperty("options", pproperties);
    widgetutils.setProperty("tooltip", pproperties);
    widgetutils.setProperty("translatedefaultvalue", pproperties);
    widgetutils.setProperty("labelwidth", pproperties);
    widgetutils.setProperty("labelalignment", pproperties);
    widgetutils.setProperty("progresstype", pproperties);
    widgetutils.setProperty("hint", pproperties);
    widgetutils.setProperty("cssclasses", pproperties);
    widgetutils.setEvent("eventstable", pproperties);
};

customwidgets.ProgressStep.updateDefaultValues = function(pobject, pparentobject) {
    var parentContainer = utils.getContainer(pparentobject).getAttribute("widgettype");
    document.getElementById("title").value ="Progress Step";
    document.getElementById("defaultvalue").value ="1 Done step";
    document.getElementById("progresstype").value ="progress-step";
}

/************************************************************************
 Below function is called while generating the widget html from IDE
 Provide the html as you want to display the widget in the generated page.
    
 In below function lcontent contains the widget related data. Access them with .propertyId
 Ex : If there is a property called title you can access it as lcontent.title
 ************************************************************************/
function getHTML(pproperties) {
 var lcontent = JSON.parse(pproperties);
 var prgTypeCls = "", toolTip = "", tooltipCls = "", cssCls = "";
 var prgType = lcontent.progresstype;
 var showClass = "";
 if(lcontent.options == "N") {
	 showClass = " shownone";
 }
 if (prgType == "1STEP") {
     prgTypeCls = "progress-step";
 } else if (prgType == "2STEP") {
     prgTypeCls = "progress-step active";
 } else {
     prgTypeCls = "progress-step incomplete";
 }
 if (lcontent.cssclasses != undefined) {
     cssCls = " " + lcontent.cssclasses;
 }
 if (lcontent.tooltip) {
    toolTip = " original-title="+lcontent.tooltip+"";
    tooltipCls = " tooltipcls";
}
 var html = "<ul id ='"+lcontent.name+"_ext' class='progress-box"+tooltipCls+cssCls+showClass+"'>"+
 "<li class='"+prgTypeCls+"'><span id ='"+lcontent.name+"'>"+lcontent.defaultvalue+"</span></li>"+
 '</ul>';
 return html;
}

function ProgressStep_displayElementsProperties(pproperties) {
    var id = document.getElementById("id").value;
	var parentContainer = utils.getContainer($("#"+id)[0]).getAttribute("widgettype");
	if (parentContainer == "TABLE") {
		$(".forTable").removeClass("shownone");//css("display", "block");
	}
	if (parentContainer == "NAVBAR") {
		$(".forNavbar").removeClass("shownone");
	}
	if (parentContainer == "FORM") {
		$(".forForm").removeClass("shownone");
	}
	//// fix 1648
	if (parentContainer == "LIST") {
		$(".forList").removeClass("shownone");
	}
	if(pproperties.draggable == "Y"){
		$(".draggableid").removeClass("shownone");
	} else {
		$(".draggableid").addClass("shownone");
	}
	if(pproperties) {
		var interfacename = pproperties.interfacename;
		var datamodeltyp = pproperties.datamodeltype;
		var nodename = pproperties.nodename;
		var elementname = pproperties.elementname;
	} else {
		var interfacename = document.getElementById("interfacename").value;
		var datamodeltyp = document.getElementById("datamodeltype").value;
		var nodename = document.getElementById("nodename").value;
		var elementname = document.getElementById("elementname").value;
	}
	if(utils.isDmlObj(interfacename,datamodeltyp, nodename, elementname)) {
		$(".handleso").addClass("disabled");
	} else {
		$(".handleso").removeClass("disabled");
	}
}

function ProgressStep_title(pobject) {
    if(pobject != undefined) {
        var res = pobject.id;
        var se = pobject.title ? pobject.title : "";
    } else {
        var res = document.getElementById("id").value;
        var se = document.getElementById("title").value;
    }
    se = widgetutils.getLITValue(se);
    var parentContainer = utils.getContainer($("#"+res)[0]);
    var parentContainerType = parentContainer.getAttribute("widgettype");
    if(parentContainerType != "TABLE") {
        var parentId = $("#"+res)[0].getAttribute("parentid");
        var parentobject = $("#" + parentId);
        if(parentobject[0].hasAttribute("role")) {
        } else
            $("#" + res).find("#label").html(se);
    } else {
        if(utils.isNull(se))
        	$("#" + res+"_heading").html("&nbsp;");
        else
        	$("#" + res+"_heading").html(se);
    }
}

function ProgressStep_headeralignment(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.headeralignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("headeralignment").value;
	}
	var index = $("#" + res).parent("tr").children().index($("#"+res));
	var temp = $("#"+($("#"+res).closest("tbody").attr("parentid")+"_header")).children()[index];
	if(se == "LEFT")
		$(temp).removeClass('tright tcenter').addClass('tleft');
	else if(se == "RIGHT")
		$(temp).removeClass('tleft tcenter').addClass('tright');
	else 
		$(temp).removeClass('tright tleft').addClass('tcenter');
}

function ProgressStep_defaultvalue(pobject) {
    if(pobject != undefined) {
        var res = pobject.id;
        var se = pobject.defaultvalue;
        var translatedefval = pobject.translatedefaultvalue;
    } else {
        var se = document.getElementById("defaultvalue").value;
        var res = document.getElementById("id").value;
        var translatedefval = document.getElementById("translatedefaultvalue").value;
    }
    if(translatedefval == "Y"){
        se = widgetutils.getLITValue(se);
    }
    $("#" + res).find("#prgid span").html(se);
}

function ProgressStep_url(pobject) {
    if(pobject != undefined) {
        var res = pobject.id;
        var se = pobject.url;
    } else {
        var se = document.getElementById("url").value;
        var res = document.getElementById("id").value;
    }
    var temp = $("#" + res).find("#hyperlink1");
    temp.attr("href", "javascript:" + se + ";");
}

function ProgressStep_labelwidth(pobject) {
    if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelwidth;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelwidth").value;
	}
	if(!utils.isNull(se) && $.isNumeric(se)){
		var ae = 100 - se;
	}else{
		var ae = 100;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
			var tem = $("#" + res).find("#content");
			if(!utils.isNull(se)) {
				$(temp).removeClass(classarray.join(' ')).addClass('w' + se);
				$(tem).removeClass(classarray.join(' ')).addClass('w' + ae);
			} else {
				$(temp).removeClass(classarray.join(' ')).addClass('w40');
				$(tem).removeClass(classarray.join(' ')).addClass('w60');
			}
		}
	}
}

function ProgressStep_displayContainerRelatedProps() {
	$("#datatype").parents("ul.control-group:first").addClass("shownone");
	$("#translatedefaultvalue").parents("ul.control-group:first").addClass("shownone");
	$("#minvalue").parents("ul.control-group:first").addClass("shownone");
	$("#maxvalue").parents("ul.control-group:first").addClass("shownone");
	$("#pattern").parents("ul.control-group:first").addClass("shownone");
	$("#mandatory").parents("ul.control-group:first").addClass("shownone");
	$("#minstringlength").parents("ul.control-group:first").addClass("shownone");
	$("#maxstringlength").parents("ul.control-group:first").addClass("shownone");
	var elmId = document.getElementById("id").value;
	var parentContainerType = utils.getContainer($("#"+elmId)[0]).getAttribute("widgettype");
	if(parentContainerType == "LIST" || parentContainerType == "NAVBAR") {
		$("#title").parents("ul.control-group:first").addClass("shownone");
	}
	var parentobject = utils.getParentObject($("#"+elmId)[0]);
	var parentId = parentobject.id;
	 if(parentobject.hasAttribute("role")) {
         //var firstChild = $(parentobject).find("#"+parentId+"_receiver").children()[0].id;
        // if(firstChild != elmId) {
         	//alert("no error before here"+$("#"+firstChild).parents("#"+parentId+"_receiver").parent().siblings().find('label').html());
         	 //$(firstChild).parents("label.elabel:first").html(se);
         	//alert($("#"+firstChild).parents("#"+parentId+"_receiver:first")[0]);
         	//$("#"+firstChild).parents("#"+parentId+"_receiver").parent().siblings().find('label').html(se);
        	 $("#title").parents("ul.control-group:first").addClass("shownone");
        // }
     }
}

function ProgressStep_labelallign(pobject) {
	if(pobject != undefined) {
		var res = pobject.id;
		var se = pobject.labelalignment;
	} else {
		var res = document.getElementById("id").value;
		var se = document.getElementById("labelalignment").value;
	}
	var parentId = $("#"+res)[0].getAttribute("parentid");
	var parentobject = $("#" + parentId);
	var parentObjType = parentobject.attr("widgettype");
	var containerobj = utils.getContainer($("#"+parentId)[0]);
	var containertype = containerobj.getAttribute("widgettype");
	var classarray = ['w0', 'w5', 'w10', 'w15', 'w20', 'w25', 'w30', 'w35', 'w40', 'w45', 'w50', 'w55', 'w60', 'w65', 'w70', 'w75', 'w80', 'w85', 'w90', 'w95', 'w100'];
	if(containertype == 'FORM'){
		if(!parentobject[0].hasAttribute("role")) {
			var temp = $("#" + res).find("#label");
        	if(se == "LEFT")
        		$(temp).removeClass('tright tcenter').addClass('tleft');
        	else if(se == "RIGHT")
        		$(temp).removeClass('tleft tcenter').addClass('tright');
        	else if(se == "CENTER")
        		$(temp).removeClass('tright tleft').addClass('tcenter');
        	else
        		$(temp).removeClass('tright tcenter tleft');
		}
	}
}

function ProgressStep_progressType(pobject) {
    if (pobject != undefined) {
        var res = pobject.id;
        var se = pobject.progresstype;
    } else {
        var se = document.getElementById("progresstype").value;
        var res = document.getElementById("id").value;
    }
    var linkElm = $("#"+res).find('#prgid');
    $(linkElm).attr('class',"");
    if (se!=""){
        if (se=="1STEP"){
            $(linkElm).addClass("progress-step");
        }else if (se=="2STEP"){
            $(linkElm).addClass("progress-step active");
        }else if (se=="CURRENTSTEP"){
            $(linkElm).addClass("progress-step incomplete");
        }
    } else {
        $(linkElm).addClass("progress-step");
    }
}
/************************************************************************
 Below function is called from layout.applyProperties() to apply the widget properties after loading the widget in designer.
 Call the functions inside below function to apply the properties (like title) to the widget.
 Pass the 'pobject' as the parameter to the functions as pobject containes the data related to that widget.
 ************************************************************************/
customwidgets.ProgressStep.applyProperties = function(pobject) {
    var parentContainer = utils.getContainer($("#"+pobject.id)[0]).getAttribute("widgettype");
    
    ProgressStep_title(pobject);
    widgetutils.setWidgetVisibility(pobject);
    ProgressStep_defaultvalue(pobject);
    ProgressStep_progressType(pobject);
    ProgressStep_url(pobject);
    if(parentContainer == "TABLE") {
        ProgressStep_headeralignment(pobject);
    }
    if(parentContainer == "FORM") {
        ProgressStep_labelwidth(pobject);
        ProgressStep_labelallign(pobject);
    }
};
